import React from 'react';
import { Link, withRouter } from 'react-router-dom';


function NavBar(props) {

    return (
            <nav className="navbar navbar-expand-lg" style={{ backgroundColor: '#76b844', height: 50 + 'px' }}>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto" >
                        <li className="nav-item" style={{ textAlign: "center" }}>
                            <Link className={"nav-link"} to={"/"}>
                                <div>
                                    <label className="mr-2 text-white" >Login</label>
                                </div>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
    )
}
export default withRouter(NavBar);

import React, { Component } from 'react';
import { Route, Switch, Link, withRouter } from 'react-router-dom';
import NavBar from "../components/NavBar"
import OffersTable from "./OffersTable";
require('../../css/app.css')

class Home extends Component {

    async componentDidMount() {
        if (this.props.location.pathname === '/callback') return;
        try {

        } catch (err) {

        }
    }

    render() {
        return (
            <div>
                <div style={{ position: 'fixed', width: '100%', top: 0 + 'px' }}>
                    <NavBar />
                </div>
                <div style={{ float: 'right', position: 'relative', width: '50%', top: 50 }}>
                    <div style={{ float: 'left' }}>
                        <Route path={"/"} component={OffersTable} />
                    </div>
                </div>

            </div>
        )
    }
}

export default withRouter(Home);

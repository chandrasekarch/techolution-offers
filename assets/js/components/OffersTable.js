import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

// this we need to configure based on the environment
const baseUrl = process.env.baseUrl;

class FetchOffer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      offers: null,
      sortType: 0
    };
    this.fetchOffers = this.fetchOffers.bind(this);
    this.sortByName = this.sortByName.bind(this);
  }

  componentWillMount() {
    this.fetchOffers();
  }

  fetchOffers(sortType) {
    if (typeof sortType == "undefined") {
      sortType = 0;
    }
    var sortParam = (sortType == 0) ? 'ASC' : 'DESC';
    axios.get(baseUrl + '/api/offers?sort=' + sortParam).then(res => {
      this.setState({ offers: res.data.data, sortType: sortType });
    })

  }

  sortByName() {
    this.fetchOffers(!this.state.sortType);
  }

  render() {
    return (
      <div class="table-responsive">
        <table class="table header-fixed" data-sort-order="desc" >
          <thead style={{ backgroundColor: '#76b844', color: 'white' }}>
            <tr>
              <th scope="col">Product  {this.state.sortType == 0 && <i class="fa fa-fw fa-sort-asc" onClick={this.sortByName}></i>}
                {this.state.sortType == 1 && <i class="fa fa-fw fa-sort-desc" onClick={this.sortByName}></i>}</th>
              <th scope="col">Cashback</th>
            </tr>
          </thead>
          {this.state.offers &&
            <tbody >
              {this.state.offers.map(data =>
                <tr>
                  <td>
                    <div>
                      <div style={{float:'left'}}>
                        <img style={{ height: 40 + 'px', width:'40px', marginRight: '10px' }} src={data.image_url} />
                      </div>
                      <div>
                        <b>{data.name}</b>
                        <div style={{marginLeft: '50px'}}>expires</div>
                      </div>
                    </div>
                  </td>
                  <td style={{ color: '#76b844' }}><b>${data.cash_back}</b><div>saved!</div></td>
                </tr>
              )}
            </tbody>
          }

        </table>
      </div>
    )

  }
}
export default withRouter(FetchOffer);

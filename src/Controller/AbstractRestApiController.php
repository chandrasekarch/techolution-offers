<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractRestApiController extends AbstractController {

    protected $request;
    protected $managerRegistry;

    protected function indexAction(Request $request, ManagerRegistry $managerRegistry) {
        $this->request = $request;
        $this->managerRegistry = $managerRegistry;
        $httpMethod = $this->request->getMethod();
        $route = $this->getRoute();

        if(!$this->isOpenApi($route, $httpMethod) && $this->isUserLoggedIn()) {
            return $this->unAuthorizedRequest();
        }

        switch ($httpMethod) {
            case 'POST':
                $data = $this->getPostData();
                return $this->create($data);
            case 'GET':
                $id = $this->getId();
                if ($id) {
                    return $this->get($id);
                }
                return $this->getList();
            case 'PUT':
                $data = $this->getPutData();
                return $this->update($id, $data);
            case 'DELETE': 
                $id = $this->getId();
                return $this->delete($id);
            default:
                return $this->methodNotAllowed();
        }
    }

    protected function create($data) {
        return $this->methodNotAllowed();
    }

    protected function getList() {
        return $this->methodNotAllowed();
    }

    protected function update($id, $data) {
        return $this->methodNotAllowed();
    }

    protected function delete($id) {
        return $this->methodNotAllowed();
    }

    protected function get($id) {
        return $this->methodNotAllowed();
    }

    protected function getParameters(Request $request)
    {
        //Need to change this to support all the request methods.
        $parameters = $request->query->all();
        return $parameters;
    }    

    protected function isOpenApi($route, $httpMethod) {
        $openApisList = $this->getOpenApisList();

        foreach($openApisList as $api) {
            if($route == $api['route'] && $httpMethod == $api['method']) {
                return true;
            }
        }

        return false;
    }

    protected function getOpenApisList() {
        return [['route' => '/api/offers', 'method' => 'GET']];
    }

    protected function methodNotAllowed() {
        return new JsonResponse([
            'success' => false,
            'code' => 405,
            'message' => 'Method not allowed',
            'error' => ['Requested method is not supported']
        ]);
    }

    protected function unAuthorizedRequest() {
        return new JsonResponse([
            'success' => false,
            'code' => 412,
            'message' => 'Unauthorized Request',
            'error' => ['Unauthorized Request']
        ]);
    }

    protected function isUserLoggedIn() {
        // implement the authentication

        return true;
    }

    protected function getId(){
        // return id from the path using request object
        return 0;
    }

    protected function getPostData() {
        //
        return [];
    }

    protected function getPutData() {
        //
        return [];
    }

    protected function getRoute() {
        // return the route using request object
        return '/api/offers';
    }

}
<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\OffersService;


class OffersController extends AbstractRestApiController
{
    /**
     * @var request Symfony\Component\HttpFoundation\Request
     * @var managerRegistry Doctrine\Common\Persistence\ManagerRegistry
     * @Route("/api/offers", name="offers")
     */

    // we can use built in rest-api bundle
    // But I dont have much to implement in the backend so implemented this for assignment
    public function restAction(Request $request, ManagerRegistry $managerRegistry)
    {
        return parent::indexAction($request, $managerRegistry);
    }

    public function getList()
    {
        $response = ['success' => false, 'data' => [], 'code' => 500];

        try {            
            $parameters = $this->getParameters($this->request);
            $offersService = new OffersService($this->managerRegistry);
            $responseOffers = $offersService->getOffers($parameters);
            $response['data'] = $responseOffers;
            $response['code'] = 200;
            $response['success'] = true;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }

        return new JsonResponse($response);
    }

}

<?php

namespace App\Service;

use App\Repository\OffersRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class OffersService
{
    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function getOffers($parameters)
    {
        try {
            $repository = new OffersRepository($this->managerRegistry);
            $offers = $repository->getOffers($parameters);
            
            return $offers;
        } // catch (\RepositoryException $e) {
            // Catching the custom exceptions
            // Log these messages using monolog
        // } 
        catch (\Exception $e) {
            // we can throw our custom exceptions here
            throw new \Exception($e->getMessage());
        }
    }
}

var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableReactPreset()

    .configureBabel(function (babelConfig) {
        babelConfig.plugins = [
            "@babel/plugin-proposal-object-rest-spread","@babel/plugin-proposal-class-properties",
            "@babel/plugin-transform-runtime"
        ]
    })
    .configureDefinePlugin((options) => {
        if(Encore.isProduction()) {
            options['process.env'].baseUrl =  JSON.stringify('http://localhost:8000');
        } else {
            options['process.env'].baseUrl =  JSON.stringify('http://localhost:8000');
        }
    })
;

var config = Encore.getWebpackConfig();
module.exports = config;